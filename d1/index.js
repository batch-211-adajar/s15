console.log("Hello World")

//JavaScript renders web pages in an interactive and dynamic fashion. Meaning, it enables us to create dynamically updating content control mutlimedia, and animate images

// Syntax, Statements, and Comments

//Statements:

//Statements in programming are instructions that we tell the computer to perform

//JS statements usually end with a semicolon (;)

//Semicolons are not required in  JS, but we will use it to help us train to locate where a statement ends

// Syntax in programming, it is the set of rules that describes how statements must be constructed

//Comments are parts of the code that gets ignored by the language

//Comments are meant to describe the written code

/* 

There are two types of comments:
1. The single-line comment (ctrl + /)
denoted by two slashes

2. Multiple-line comment (ctrl + shift + /)

*/

//Variables

// It is used to contain data
// This makes it easier for us to associat information stored in our devices to actual "names" about information

//Declaring variables




		let productName = 'desktop computer';
		console.log(productName);

		let productPrice = 18999;

	console.log(productPrice)

	const interest = 3.539;
	// const pi = 3.1416;

	productName = 'laptop'
	console.log(productName)

	let mailAddress = "Metro Manila\n\nPhilippines";
	console.log(mailAddress);



	let person= {

		fullName: "Edward Scissorhands",
		age: 25,
		contact: [""]



	}


	let myGrades = {
		firstGrading: 98.7,
		secondGrading: 92.1,
		thirdGrading: 90.2,
		fourthGrading: 94.6,

	}

	console.log(myGrades);